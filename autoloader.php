<?php
spl_autoload_register('autoLoader');

/**
 * Autoload classes and containers without include line
 */
function autoLoader($className)
{
    $path = "classes/";
    $ext = ".class.php";
    $fullPath = $path . $className . $ext;

    if(ifPathExists("classes/", $className, ".class.php") != false)
    {
        include_once $fullPath;
    }
    elseif(ifPathExists("containers/", $className, ".cont.php") != false)
    {
        $fullPath = ifPathExists("containers/", $className, ".cont.php");
        include_once $fullPath;
    }
    else
    {
        return false;
    }
    // include_once $fullPath;
}

function ifPathExists($folder, $className, $ext){
    $fullPath = $folder . $className . $ext;
    if(file_exists($fullPath))
    {
        return $fullPath;
    }
    else
    {
        return false;
    }
}

?>