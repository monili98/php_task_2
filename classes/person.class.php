<?php

class Person {
    public $first_name;
    public $age;
    public $gender;

    public function __construct($first_name, $age, $gender)
    {
        $this->first_name = $first_name;
        $this->age = $age;
        $this->gender = $gender;
    }

    public function printPerson()
    {
        echo $this->first_name . " is " . $this->age . " years old " . $this->gender . ".";
    }
}

?>