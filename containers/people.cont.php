<?php

class People {
    public $people_array = array();
    public $counter = 0;

    // There is no overloading in PHP; you cannot have methods with the same names but different parameters, therefore we equate parameter to null
    public function __construct(Person $person = null)
    {
        if($person != null)
        {
            array_push($this->people_array, $person); //pushes the passed variables into the end of array
        }
    }

    public function addPerson(Person $person)
    {
        array_push($this->people_array, $person);
    }

    public function getPeople()
    {
        $people = $this->people_array;
        return $people;
    }

    public function printPeople()
    {
        echo "Printing people array (", $this->countPeople() ,"):<br>";
        foreach($this->people_array as $person){
            print_r($person);
            echo "<br>";
        }
    }

    public function countPeople()
    {
        return count($this->people_array);
    }
}

?>