<?php

/**
 * Convert associative array into csv file
 * @param $file_path - given file path
 */
function assoc_to_csv($file_path, $data_assoc)
{
    $handle = fopen($file_path, 'w') or die("Can't open file");
    $titles = array_keys($data_assoc[0]); //get keys of assoc array
    fputcsv($handle, $titles); //put assoc keys (titles) into csv
    foreach($data_assoc as $person)
    {
        fputcsv($handle, $person); //put assoc values (person details) into csv
    }
    echo "Given associative array successfully converted into csv file. File location: ", $file_path, ".<br>";
    fclose($handle);
}

/**
 * Convert associative array into xml file;
 * @param $file_path - given file path
 */
function assoc_to_xml($file_path, $data_assoc)
{
    $xml_doc = new DOMDocument(); //to create blank xml document (DOMDocument Object)

    $people_count = count($data_assoc); //count people in array
    
    $root = $xml_doc->appendChild($xml_doc->createElement("users")); //to create users tag
    $root->appendChild($xml_doc->createElement("total_rows", $people_count)); //to create total rows tag (how much users rows exist)
    $tabUsers = $root->appendChild($xml_doc->createElement("rows")); //to create rows tag

    foreach($data_assoc as $user) //go through users
    {
        if(!empty($user))
        {
            $tabUser = $tabUsers->appendChild($xml_doc->createElement("user")); //to create user tag
            foreach($user as $key=>$val) //to print person details
            {
                $tabUser->appendChild($xml_doc->createElement($key, $val)); //to create key tag and put value
            }
        }
    }
    header("Content-Type: text/plain");
    $xml_doc->formatOutput = true; //to make output pretty
    $result = $xml_doc->save($file_path);//save xml file

    if($result!= false) //to check if saving to xml was successful
    {
        echo "Given associative array successfully converted into xml file. File location: ", $file_path;
    }
}

/**
 * Convert associative array into json file
 * @param $file_path - given file path
 */
function assoc_to_json($file_path, $data_assoc)
{
    $assoc_json = json_encode($data_assoc);
    if($assoc_json == false) //convert assoc to array failed
    {
        echo "Error occurred while trying to convert xml into an object.<br>";
    }
    else //convert assoc to array successful
    {
        if(file_put_contents($file_path, $assoc_json)) //write json to file successful
        {
            echo "Given associative array successfully converted into json file. File location: ", $file_path, ".<br>";
        }
        else //write json to file error
        {
            echo "Error occurred while trying to create json file.<br>";
        }
    }
}

?>