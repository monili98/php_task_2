<?php
    require 'vendor/autoload.php';
    include 'autoloader.php';

    // --- CONNECT TO MongoClient (MongoShell)
    $client = new MongoDB\Client();

    // --- TO CREATE DB (IF IT IS ALREADY EXISTS, THEN SELECT IT)
    $php_task_db = $client->php_task_db; //Create DB with name "php_task_db"

    // --- TO CREATE COLLECTION
    $collection_people_name = "people";

    $if_exists = false;
    foreach($php_task_db->listCollections() as $collection)
    {
        if($collection["name"] == $collection_people_name) // if desired collection already exist
        {
            $if_exists = true;
            echo "Trying to create collection '", $collection_people_name, "', but it is already exists.<br>";
        }
    }
    if(!$if_exists){
        $people_col = $php_task_db->createCollection($collection_people_name); //Create Collection (as table in SQL)
        echo "Collection '", $collection_people_name, "' was successfully created.<br>";
    }
    $people_collection = $php_task_db->people; //Select existing collection

    // --- DATA
    //given assoc array
    $data_assoc = [
        [
            'first_name' => 'Kiestis',
            'age' => 29,
            'gender' => 'male'
        ],
        [
            'first_name' => 'Vytska',
            'age' => 32,
            'gender' => 'male'
        ],
        [
            'first_name' => 'Karina',
            'age' => 25,
            'gender' => 'female'
        ],
    ];

    // --- TO INSERT DOCUMENT
    $titles = array_keys($data_assoc[0]); //get keys of assoc array
    $people = new People(); 
    foreach($data_assoc as $person)
    {
        // $document = array(
        //     $titles[0] => $person[$titles[0]], 
        //     $titles[1] => $person[$titles[1]], 
        //     $titles[2] => $person[$titles[2]]
        // );
        $person_document = new Person($person[$titles[0]], $person[$titles[1]], $person[$titles[2]]); //create person object (Member of Person class)        
        
        // var_dump($insertOneResult);
        $is_exists = $people_collection->findOne($person_document); //searching for document in collection
        if(!$is_exists){ //if document doesn't exist in the collection
            // $insertOneResult = $people_collection->insertOne($person_document); // insert document into collection
            $people->addPerson($person_document); //add person object to People Container
            echo "New document: '", $person_document->printPerson(), "' was succesfully inserted<br>";
        }
        else{
            echo "Trying to insert document: '", $person_document->printPerson(), "', but it is already exists.<br>";
        }
    }
    $inserted_rows_count = $people->countPeople();
    if($inserted_rows_count != 0)
    {
        // echo "<br>", $people->printPeople();
        echo $inserted_rows_count, " documents was inserted.<br>";
    }
    else
    {
        echo "Nothing was inserted.<br>";
    }

    // --- TO DELETE COLLECTION (WITH ALL ITS DOCUMENTS)
    // $coll_drop_result = $php_task_db->dropCollection($collection_people_name);
    // if($coll_drop_result)
    // {
    //     echo "Collection '", $collection_people_name, "' was successfully dropped.<br>";
    // }
    // else
    // {
    //     echo "Failed to drop collection '", $collection_people_name, "'.<br>";
    // }

    // --- QUERY DOCUMENTS ---

    // --- FIND ONE (FIRST)
    $document_by_name = $people_collection->findOne(['first_name' => 'Karina']);
    // var_dump($document_by_name);

    // --- FIND ALL MATCHES
    $document_list_by_gender = $people_collection->find(
        ['gender' => 'male'],
        ['projection' => ['_id' => 0, 'first_name' => 1]] // to show only first_name field of document (0-hide; 1-show)
    );
    foreach($document_list_by_gender as $doc)
    {
        //var_dump($doc);
    }

    $document_list_by_age = $people_collection->find(['age' => ['$gt' => 28]]); //gt - greater than?
    foreach($document_list_by_age as $doc)
    {
        // var_dump($doc);
    }

    // --- UPDATE ONE DOCUMENT (FIRST MATCHED)
    /*echo "Document before update:<br>";
    $document_to_update = $people_collection->findOne(['first_name' => 'Karina']);
    var_dump($document_to_update);
    $update_result = $people_collection->updateOne(
        ['first_name' => 'Karina'],
        ['$set' => ['age' => 28]]
    );
    if($update_result)
    {
        printf("<br>Matched %d documents \n", $update_result->getMatchedCount());
        printf("<br>Modified %d documents \n", $update_result->getModifiedCount());
        echo "<br>Document after update:<br>";
        $updated_document = $people_collection->findOne(['first_name' => 'Karina']);
        var_dump($updated_document);
    }
    else
    {
        echo "<br>Something went wrong while trying to update document...<br><br>";
    }*/

    // --- UPDATE MANY DOCUMENTS
    /*echo "Documents before update:<br>";
    $documents_to_update = $people_collection->find(['gender' => 'male']);
    foreach($documents_to_update as $doc)
    {
        var_dump($doc);
    }
    $update_result = $people_collection->updateMany(
        ['gender' => 'male'],
        ['$set' => ['age' => '33', 'manager' => 'Rob']] //update age field and add new field manager
    );
    if($update_result)
    {
        printf("<br>Matched %d documents \n", $update_result->getMatchedCount());
        printf("<br>Modified %d documents \n", $update_result->getModifiedCount());
        echo "<br>Document after update:<br>";
        $updated_documents = $people_collection->find(['gender' => 'male']);
        foreach($updated_documents as $doc)
        {
            var_dump($doc);
        }
    }
    else
    {
        echo "<br>Something went wrong while trying to update documents...<br><br>";
    }*/

    // --- REPLACE DOCUMENT (to replace entire document with new one)
    /*echo "Document before replacement:<br>";
    $document_to_be_replaced = $people_collection->find(['first_name' => 'Vytska']);
    var_dump($document_to_be_replaced);
    $replace_result = $people_collection->replaceOne(
        ['first_name' => 'Vytska'],
        ['first_name' => 'Vytautas', 'fav_colour' => 'yellow']
    );
    printf("<br>Matched %d documents \n", $replace_result->getMatchedCount());
    printf("<br>Modified %d documents \n", $replace_result->getModifiedCount());
    echo "<br>All documents after replacement:<br>";
    $replaced_document = $people_collection->find();
    foreach($replaced_document as $doc)
    {
        var_dump($replaced_document);
    }*/

    //Quick insert
    /*$insertOneResult = $people_collection->insertOne(
        [
            'first_name' => 'Vladas',
            'age' => 23,
            'gender' => 'male'
        ]
    );*/

    // --- LIMIT THE NUMBER OF RETRIEVED DOCUMENTS
    $all_documents = $people_collection->find(
        [], // to not specify any condition (find all documents of collection)
        [
            'limit' => 7, //to retrieve max 2 documents
            'skip' => 1, //first skip 2 (and only then get limit number of elements)
            'sort' => ['first_name' => 1] //sort sliced documents by field (-1 is descending; 1 - ascending)
        ]
    ); 
    foreach($all_documents as $doc)
    {
        var_dump($doc);
    }
?>