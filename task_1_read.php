<?php
include 'autoloader.php';
libxml_use_internal_errors(TRUE);

// --- CORRECT FILES ---
// $file_name = 'file_1.csv';
// $file_name = 'file_2.xml';
// $file_name = 'file_3.json';

// --- INCORRECT FILES ---
// $file_name = 'file_4.png';
// $file_name = 'empty.csv';
// $file_name = 'incorrect.xml';
// $file_name = 'incorrect.json';
// $file_name = 'not_exists.csv';


if (isset($file_name)) //is filename was given
{
    $file_path = 'files_to_read//' . $file_name;
    if (file_exists($file_path) == false) //is given file exist in project directory
    {
        echo "File ", $file_name, " does not exist in the project directory.";
        exit();
    }
} 
else 
{
    echo "File name was not given.";
    exit();
}

$allowed_formats = array('csv', 'xml', 'json');
$ext = pathinfo($file_name, PATHINFO_EXTENSION); //get given file format

if (!in_array($ext, $allowed_formats)) //file is not allowed
{
    echo $ext, " file format is not allowed.<br>";
} 
else //file is allowed
{
    if (filesize($file_path) == 0) //file is empty
    {
        echo "File is empty.";
    } 
    else 
    { //file has content
        echo "Given file: ", $file_name, "<br>";
        if ($ext == 'csv') //if file type is csv
        {
            csv_to_assoc($file_path);
        } 
        elseif ($ext == 'xml') //if file type is xml
        {
            xml_to_assoc($file_path);
        } 
        else //if file type is json
        {
            json_to_assoc($file_path);
        }
    }
}

/**
 * Convert csv file into associative array
 * @param $file_path - given file path
 */
function csv_to_assoc($file_path)
{
    echo "File content:<br>";
    $csv_file = file($file_path); //read all file into array
    $titles = explode(',', $csv_file[0]); //column names into array
    $people = array_slice($csv_file, 1); //people array (all file except first row)

    $assoc_array = [];
    foreach ($people as $person)
    {
        $person_details = explode(',', $person); //person details to temporary array

        $person_assoc = [];
        for ($i = 0; $i < count($titles); $i++) 
        {
            $person_assoc[$titles[$i]] = $person_details[$i]; //person details into assoc array
        }
        $assoc_array[count($assoc_array)] = $person_assoc; //person into people assoc array
    }
    print("<pre>" . print_r($assoc_array, true) . "</pre>");
}

/**
 * Convert xml file into associative array
 * @param $file_path - given file path
 */
function xml_to_assoc($file_path)
{
    $xml_file = file_get_contents($file_path); //read file into string
    $xml_file_object = simplexml_load_string($xml_file); //convert string into object
    if ($xml_file_object == false) 
    {
        echo "Error occurred while trying to convert xml into an object.<br>";
    } 
    else 
    {
        echo "File content:<br>";
        $xml_file_json = json_encode($xml_file_object); //convert object into json
        $xml_file_assoc = json_decode($xml_file_json, true); //convert json into associative array(true param)
        print("<pre>" . print_r($xml_file_assoc, true) . "</pre>");
    }
}

/**
 * Convert json file into associative array
 * @param $file_path - given file path
 */
function json_to_assoc($file_path)
{
    $json_file = file_get_contents($file_path);
    $json_file_assoc = json_decode($json_file, true);
    if ($json_file_assoc == null) 
    {
        echo "Error occurred while trying to convert json into an associative array.<br>";
    } 
    else 
    {
        echo "File content:<br>";
        print("<pre>" . print_r($json_file_assoc, true) . "</pre>");
    }
}
