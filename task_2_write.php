<?php
    include 'includes\write_to_file_functions.inc.php';
    include 'autoloader.php';

    // --- CORRECT FILENAMES ---
    // $file_name = 'assoc.csv';
    $file_name = 'assoc.xml';
    // $file_name = 'assoc.json';

    // --- INCORRECT FILESNAMES ---
    // $file_name = 'assoc.png';

    //given assoc array
    $data_assoc = [
        [
            'first_name' => 'Kiestis',
            'age' => 29,
            'gender' => 'male'
        ],
        [
            'first_name' => 'Vytska',
            'age' => 32,
            'gender' => 'male'
        ],
        [
            'first_name' => 'Karina',
            'age' => 25,
            'gender' => 'female'
        ],
    ];

    $titles = array_keys($data_assoc[0]); //get keys of assoc array
    $people = new People(); 
    foreach($data_assoc as $person)
    {
        $person_obj = new Person($person[$titles[0]], $person[$titles[1]], $person[$titles[2]]); //create person object (Member of Person class)
        $person_obj->printPerson();
        
        $people->addPerson($person_obj); //add person object to People Container
    }
    echo "<br>";
    $people->printPeople();
    

    $ext = '';
    $file_path = '';

    if(isset($file_name)) //check if filename was given
    {
        $file_path = 'writed_files//' . $file_name;

        $allowed_formats = array('csv', 'xml', 'json');
        $ext = pathinfo($file_name, PATHINFO_EXTENSION); //get given file format

        if (!in_array($ext, $allowed_formats)) //file is not allowed
        {
            echo $ext, " file format is not allowed.<br>";
            exit();
        }
    }
    else
    {
        echo "File name was not given.";
        exit();
    }

    if(count($data_assoc) == 0) //if assoc array is empty
    {
        echo "Given associative array is empty.<br>";
    }
    else
    { //if assoc array has content

        if($ext == 'csv')
        {
            assoc_to_csv($file_path, $data_assoc); //function from included file
        }  
        elseif($ext == 'xml')
        {
            //printing xml format to screen
            assoc_to_xml($file_path, $data_assoc); //function from included file
        }   
        else //if file type is json
        {
            assoc_to_json($file_path, $data_assoc); //function from included file
        }   
    }
?>