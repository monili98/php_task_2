<!DOCTYPE html>
<html> 
    <head>
        <meta charset="UTF-8">
        <title>task_3_http</title>
    </head>
    
    <body>
        <p>
        Given data:
`   <?php
    include 'includes\write_to_file_functions.inc.php';
    include 'autoloader.php';

    //given assoc array
    $data_assoc = [
        [
            'first_name' => 'Kiestis',
            'age' => 29,
            'gender' => 'male'
        ],
        [
            'first_name' => 'Vytska',
            'age' => 32,
            'gender' => 'male'
        ],
        [
            'first_name' => 'Karina',
            'age' => 25,
            'gender' => 'female'
        ],
    ];

    print("<pre>".print_r($data_assoc,true)."</pre>");

    if(is_array($data_assoc)) //is given array truly is array
    {
        foreach($data_assoc as $person)
        {
            if(array_values($person) === $person) //is given array is an associative
            {
                echo "Given array is not an associative array.";
                exit();
            }
        }
    }
    else 
    {
        echo "Given data is not an array.";
        exit();
    }

    $titles = array_keys($data_assoc[0]); //get keys of assoc array
    $people = new People(); 
    foreach($data_assoc as $person)
    {
        $person_obj = new Person($person[$titles[0]], $person[$titles[1]], $person[$titles[2]]); //create person object (Member of Person class)
        $person_obj->printPerson();
        
        $people->addPerson($person_obj); //add person object to People Container
    }
    echo "<br>";
    $people->printPeople();
    ?>
        <p>Choose file type to which you want to convert an associative array:</p>

        <form action="task_3_http.php" method="post">
        <select id="file_type" name="file_type">
            <option value="csv" selected>csv</option>
            <option value="xml">xml</option>
            <option value="json">json</option>
        </select>
        <input type="submit" name="submit" value="Select">
        </form>
    </body>
</html>

<?php
    $file_type = '';
    if(isset($_POST['submit'])) 
    {
        $file_type = $_POST['file_type'];
        echo "Selected file type was " . $file_type . ".<br>";

        $file_name = 'http_assoc.' . $file_type;
        
        if(isset($file_name)) //check if filename was given
        {
            $file_path = 'writed_files_http//' . $file_name;
        }
        else
        {
            echo "File name was not given.";
            exit();
        }

        if(count($data_assoc) == 0) //if assoc array is empty
        {
            echo "Given associative array is empty.<br>";
        }
        else //if assoc array has content
        {
            if($file_type == 'csv')
            {
                assoc_to_csv($file_path, $data_assoc); //function from included file
                echo "Or you can ";
                //to download converted data
                ?> 
                <a href="writed_files_http//http_assoc.csv" download="assoc_http.csv">Download array converted to .csv file</a> 
                <?php
                echo " now.";
            }  
            elseif($file_type == 'xml')
            {
                assoc_to_xml($file_path, $data_assoc); //function from included file 
            }   
            else //if file type is json
            {
                assoc_to_json($file_path, $data_assoc); //function from included file
                echo "Or you can ";
                ?>
                <a href="writed_files_http//http_assoc.json" download="assoc_http.json">Download array converted to .json file</a>
                <?php
                echo " now.";
            }   
        }          
    }    
?>
